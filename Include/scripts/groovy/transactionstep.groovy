import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class transactionstep {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I navigate to credit card page")
	def navigatetocreditcardpage() {
		println ("\n I am inside payment with credit card page")
		WebUI.openBrowser('')

		WebUI.navigateToUrl('https://demo.midtrans.com/')

		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_BUY NOW'))
		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_CHECKOUT'))
		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Continue'))
		WebUI.delay(3)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_Credit Card'))
	}

	@When("I input number in card number (.*), expiry date (.*), cvv (.*)")
	def R (String cardnumber,expirydate,cvv) {
		println ("I want to input information to payment with credit card")
		println ("Input credit card number: "+cardnumber)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_sample-store-1582045011_cardnumber'), cardnumber)
		WebUI.delay(2)
		println ("Input expiry date card: "+expirydate)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input'), expirydate)
		WebUI.delay(2)
		println ("Input cvv: "+cvv)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_1'), cvv)
		WebUI.delay(2)
	}

	@And("I click button pay now")
	def clickpaynow() {
		println ("process to payment")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Pay Now'))
		WebUI.delay(3)
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sample Store/input_Password_PaRes'), '4tAN/DuJV7Y=')
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/button_OK'))
		WebUI.delay(3)
	}

	@Then("I have success transaction for payment")
	def successcreditcardpayment () {
		println ("transaction success")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_Transaction successful'))
		WebUI.delay(3)
	}

	@Then("I have failed transaction for payment")
	def failedcreditcardpayment () {
		println ("transaction failed")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/span_Transaction failed'))
		WebUI.delay(3)
	}
}