#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@trasaction
Feature: Payment with credit card
  when the buyer wants to make a payment with a credit card

  @success
  Scenario Outline: credit card numbers can be used
    Given I navigate to credit card page
    When I input number in card number <cardnumber>, expiry date <expirydate>, cvv <cvv>
    And I click button pay now
    Then I have success transaction for payment

    Examples: 
      | cardnumber  				| expirydate | cvv  | otp    | status |
      | 4811 1111 1111 1114 |    02/20   | 123  | 112233 | success|
      
   @failed
   Scenario Outline: credit card numbers can't be used
   Given I navigate to credit card page
   When I input number in card number <cardnumber>, expiry date <expirydate>, cvv <cvv>
   And I click button pay now
   Then I have failed transaction for payment   
   
   Examples: 
      | cardnumber  				| expirydate | cvv  | otp    | status |
      | 4911 1111 1111 1113 |    02/20   | 123  | 112233 | failed |
      